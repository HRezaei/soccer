/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Soccer;

/**
 *
 * @author Rezaei
 */
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImagePanel extends JPanel{

    private BufferedImage image;

    public File Image=new File("");

    public File getImage() {
        return Image;
    }

    public void setImage(File Image) {
        this.Image = Image;
        try{
            image = ImageIO.read(Image);
        }catch (IOException ex) {
            // handle exception...
       }
    }

    
    
    public ImagePanel() {
       try {                
          image = ImageIO.read(Image);
       } catch (IOException ex) {
            // handle exception...
       }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(image!=null)g.drawImage(image, 0, 0, null); // see javadoc for more info on the parameters            
    }

}