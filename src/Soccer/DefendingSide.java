/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Soccer;

/**
 *
 * @author Rezaei
 */

public enum DefendingSide {
    /**
     * Indicates the team is defending the east side.
     */
    Left,
    /**
     * Indicates the team is defending the west side.
     */
    Right;

    /**
     * Gets the opposite side of the given direction. For example,
     * DefendingSide.Left.getOppositeSide() returns DefendingSide.Right.
     * @return the opposite defending side.
     */
    public DefendingSide getOppositeSide() {
		switch (this) {
		case Left : return Right;
		case Right : return Left;
		default : throw new RuntimeException("Unknown side " + this);
		}
	}
}
