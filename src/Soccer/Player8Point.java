/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Soccer;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JRadioButton;
import javax.swing.Timer;

/**
 *
 * @author Rezaei
 */
public class Player8Point extends JRadioButton {

    private Timer animationTimer; // Timer drives animation
    private String Status = "";
protected Team InTeam;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
        this.setText(Status);
    }
    private Point To;
    private double Teta;
    private Pitch OnPitch;

    public Player8Point() {
        this.animationTimer = new Timer(100, new TimerHandler());
    }

    public void Play(Pitch P) {
        OnPitch = P;
        animationTimer.start();
    }

    private void MakeDecision() {
        if (getLocation().distance(OnPitch.ball.getLocation().x, OnPitch.ball.getLocation().y) == 0) {
            Shoot(OnPitch.getGoal(this.InTeam.Side));
        } else /*if (!Status.startsWith("Moving"))*/ {
            Move(OnPitch.ball.getLocation());
        }
    }

    public void Move(java.awt.Point p) {
        setStatus("Moving to " + p.x + "," + p.y);
        To = p;
        //From = this.getLocation();
    }

    private void Shoot(Point to) {
        setStatus("Shooting");
        OnPitch.ball.TakeImpact(to,100);
    }

    public void nextFrame() {

        if (Status.startsWith("Moving")) {
            if (this.getLocation().distance(To.x, To.y) != 0) {
                Point from = this.getLocation();
                Point[] Nears = {new Point(from.x + 1, from.y), new Point(from.x + 1, from.y + 1), new Point(from.x, from.y + 1), new Point(from.x - 1, from.y+1), new Point(from.x - 1, from.y ), new Point(from.x-1, from.y -1), new Point(from.x, from.y - 1), new Point(from.x+1, from.y - 1)};
                double[] Distances = {To.distance(Nears[0]), To.distance(Nears[1]), To.distance(Nears[2]), To.distance(Nears[3]), To.distance(Nears[4]), To.distance(Nears[5]),To.distance(Nears[6]),To.distance(Nears[7])};
                this.setLocation(Nears[Utility.min(Distances)]);
            }
        } else {
            int u = 0;
        }
    
    /*
     images[ currentImage ].paintIcon( this, this.getGraphics(), 0, 0 );

     // set next image to be drawn only if timer is running
     if ( animationTimer.isRunning() )
     currentImage = ( currentImage + 1 ) % TOTAL_IMAGES;*/
}
// start animation, or restart if window is redisplayed
public void startAnimation() {
        if (animationTimer.isRunning()) {
            animationTimer.restart();
        } // end if
        else // animationTimer already exists, restart animation
        {
            //currentImage = 0; // display first image
            animationTimer.start(); // start timer
        } // end else
    } // end method startAnimation

    // stop animation timer
    public void stopAnimation() {
        animationTimer.stop();
    





} // end method stopAnimation

    // inner class to handle action events from Timer
    private class TimerHandler implements ActionListener {
    // respond to Timer's event

    public void actionPerformed(ActionEvent actionEvent) {
        MakeDecision();
        nextFrame(); // repaint animator
        // AirPortApp.getApplication().View.getConsole().Log("animation frame");
    } // end method actionPerformed
}
}
